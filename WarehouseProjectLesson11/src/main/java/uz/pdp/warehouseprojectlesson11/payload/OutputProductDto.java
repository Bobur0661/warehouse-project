package uz.pdp.warehouseprojectlesson11.payload;

import lombok.Data;

import java.util.Date;

@Data
public class OutputProductDto {

    private Long id;
    private Double price;
    private Double amount;
    private Long productId;
    private Long outputId;

}
