package uz.pdp.warehouseprojectlesson11.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.warehouseprojectlesson11.entity.InputProduct;
import uz.pdp.warehouseprojectlesson11.entity.OutputProduct;
import uz.pdp.warehouseprojectlesson11.payload.ApiResponse;
import uz.pdp.warehouseprojectlesson11.payload.InputProductDto;
import uz.pdp.warehouseprojectlesson11.payload.OutputDto;
import uz.pdp.warehouseprojectlesson11.payload.OutputProductDto;
import uz.pdp.warehouseprojectlesson11.repository.InputProductRepo;
import uz.pdp.warehouseprojectlesson11.repository.OutputProductRepo;
import uz.pdp.warehouseprojectlesson11.service.InputProductService;
import uz.pdp.warehouseprojectlesson11.service.OutputProductService;

import java.util.List;

@RestController
@RequestMapping("/outputProduct")
public class OutputProductController {

    @Autowired
    OutputProductService outputProductService;

    @Autowired
    OutputProductRepo outputProductRepo;


    @GetMapping
    public List<OutputProduct> get() {
        return outputProductRepo.findAll();
    }


    @PostMapping("/addOrUpdate")
    public ApiResponse addOrUpdate(@RequestBody OutputProductDto dto) {
        return outputProductService.addOrUpdate(dto);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return outputProductService.delete(id);
    }


}
