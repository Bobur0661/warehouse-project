package uz.pdp.warehouseprojectlesson11.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.warehouseprojectlesson11.entity.Measurement;
import uz.pdp.warehouseprojectlesson11.payload.ApiResponse;
import uz.pdp.warehouseprojectlesson11.repository.MeasurementRepo;
import uz.pdp.warehouseprojectlesson11.service.MeasurementService;

import java.util.List;

@RestController
@RequestMapping("/measurement")
public class MeasurementController {

    @Autowired
    MeasurementService measurementService;

    @Autowired
    MeasurementRepo measurementRepo;


    @GetMapping("/all")
    public List<Measurement> get() {
        return measurementRepo.findAll();
    }


    @PostMapping("/addOrEdit")
    public ApiResponse addMeasurement(@RequestBody Measurement measurement) {
        return measurementService.addOrEdit(measurement);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return measurementService.delete(id);
    }

}
