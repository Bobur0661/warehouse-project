package uz.pdp.warehouseprojectlesson11.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.warehouseprojectlesson11.entity.Supplier;
import uz.pdp.warehouseprojectlesson11.payload.ApiResponse;
import uz.pdp.warehouseprojectlesson11.repository.SupplierRepo;
import uz.pdp.warehouseprojectlesson11.service.SupplierService;

import java.util.List;

@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    SupplierService supplierService;

    @Autowired
    SupplierRepo supplierRepo;


    @GetMapping("/all")
    public List<Supplier> get() {
        return supplierRepo.findAll();
    }


    @PostMapping("/add")
    public ApiResponse add(@RequestBody Supplier supplier) {
        return supplierService.add(supplier);
    }


    @PutMapping("/update/{id}")
    public ApiResponse update(@PathVariable Long id, @RequestBody Supplier supplier) {
        return supplierService.update(id, supplier);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return supplierService.delete(id);
    }

}
