package uz.pdp.warehouseprojectlesson11.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.warehouseprojectlesson11.entity.*;
import uz.pdp.warehouseprojectlesson11.payload.ApiResponse;
import uz.pdp.warehouseprojectlesson11.payload.InputProductDto;
import uz.pdp.warehouseprojectlesson11.payload.OutputProductDto;
import uz.pdp.warehouseprojectlesson11.repository.*;

import java.util.Optional;

@Service
public class OutputProductService {

    @Autowired
    OutputProductRepo outputProductRepo;

    @Autowired
    ProductRepo productRepo;

    @Autowired
    OutputRepo outputRepo;


    public ApiResponse addOrUpdate(OutputProductDto dto) {
        OutputProduct outputProduct = new OutputProduct();
        if (dto.getId() != null) {
            outputProduct = outputProductRepo.getById(dto.getId());
        }
        Optional<Product> optionalProduct = productRepo.findById(dto.getProductId());
        if (!optionalProduct.isPresent()) {
            return new ApiResponse(false, "Product Not Found!");
        }
        Optional<Output> optionalOutput = outputRepo.findById(dto.getOutputId());
        if (!optionalOutput.isPresent()) {
            return new ApiResponse(false, "Output not Found!");
        }

        outputProduct.setOutput(optionalOutput.get());
        outputProduct.setProduct(optionalProduct.get());
        outputProduct.setAmount(dto.getAmount());
        outputProduct.setPrice(dto.getPrice());
        outputProductRepo.save(outputProduct);
        return new ApiResponse(true, dto.getId() != null ? "Edited" : "Saved");
    }


    public ApiResponse delete(Long id) {
        try {
            outputProductRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on Deleting!");
        }
    }


}
