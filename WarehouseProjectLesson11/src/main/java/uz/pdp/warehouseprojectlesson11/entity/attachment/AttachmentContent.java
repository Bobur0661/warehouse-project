package uz.pdp.warehouseprojectlesson11.entity.attachment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.warehouseprojectlesson11.entity.temlate.AbsId;
import uz.pdp.warehouseprojectlesson11.entity.temlate.AbsNameId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class AttachmentContent extends AbsId {


    @Column(nullable = false)
    private byte[] bytes;

    @OneToOne
    private Attachment attachment;

}
