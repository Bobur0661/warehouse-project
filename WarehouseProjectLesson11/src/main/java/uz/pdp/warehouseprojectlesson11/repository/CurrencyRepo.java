package uz.pdp.warehouseprojectlesson11.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.warehouseprojectlesson11.entity.Currency;
import uz.pdp.warehouseprojectlesson11.entity.Measurement;

public interface CurrencyRepo extends JpaRepository<Currency, Long> {


    boolean existsByName(String name);

}
